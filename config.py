import os


class Config:
    DEBUG = False
    SECRET_KEY = os.getenv("SECRET_KEY")
