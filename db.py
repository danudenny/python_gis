from os.path import join, dirname
import mysql.connector
import os
from dotenv import load_dotenv, dotenv_values
from environs import Env

dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)

db_config = {
    "host": os.getenv("DB_HOST"),
    "port": int(os.getenv("DB_PORT")),
    "user": os.getenv("DB_USER"),
    "password": os.getenv("DB_PASSWORD"),
    "database": os.getenv("DB_DATABASE"),
}


def create_db_connection():
    try:
        connection = mysql.connector.connect(**db_config)
        print("Database connection successful")
        return connection
    except mysql.connector.Error as err:
        print(f"Error: {err}")
        return None
