from flask import Flask, jsonify
from db import create_db_connection
from config import Config
import json
import folium as fl
from functools import reduce

app = Flask(__name__)
app.config.from_object(Config)

db = create_db_connection()


@app.route('/check-health', methods=['GET'])
def check_health():
    return jsonify({"status": "OK"})


def graham_scan(points):
    TURN_LEFT, TURN_RIGHT, TURN_NONE = (1, -1, 0)

    def cmp(a, b):
        return (a > b) - (a < b)

    def turn(p, q, r):
        return cmp((q[0] - p[0]) * (r[1] - p[1]) - (r[0] - p[0]) * (q[1] - p[1]), 0)

    def _keep_left(hull, r):
        while len(hull) > 1 and turn(hull[-2], hull[-1], r) != TURN_LEFT:
            hull.pop()
        if not len(hull) or hull[-1] != r:
            hull.append(r)
        return hull

    sorted_points = sorted(points)
    l = reduce(_keep_left, sorted_points, [])
    u = reduce(_keep_left, reversed(sorted_points), [])
    return l.extend(u[i] for i in range(1, len(u) - 1)) or l


@app.route('/convex_hull', methods=['GET'])
def convex_hull_api():
    cursor = db.cursor(dictionary=True)

    cursor.execute(
        "SELECT SupplierGroupID, RegID, ST_AsGeoJson(Polygon) as Polygon FROM kol_mis.ktv_supplier_group WHERE Polygon IS NOT NULL")
    result = cursor.fetchall()

    coordinates = []
    grouped_coordinates = {}
    m = fl.Map(location=[-2.4833826, 117.8902853], zoom_start=5)

    for row in result:
        reg_id = row['RegID']
        coordinates = json.loads(row['Polygon'])['coordinates'][0]
        swapped_coordinates = [[coord[1], coord[0]] for coord in coordinates]

        fl.Marker(swapped_coordinates[0], popup=fl.Popup(str(reg_id), parse_html=True)).add_to(m)
        fl.Polygon(swapped_coordinates, color='red', weight=1, opacity=1, fill=True, fill_color='green',
                   fill_opacity=0.4).add_to(m)

        if reg_id not in grouped_coordinates:
            grouped_coordinates[reg_id] = []

        grouped_coordinates[reg_id].append(swapped_coordinates)

    for reg_id, swapped_coordinates in grouped_coordinates.items():
        joined_coordinates = reduce(lambda x, y: x + y, swapped_coordinates)
        scanned = graham_scan(joined_coordinates)

        fl.Polygon(scanned, color='blue', weight=1.2, opacity=1, fill=True, fill_color='blue', fill_opacity=0.4).add_to(
            m)

    geojson_result = {
        "type": "FeatureCollection",
        "features": []
    }

    for reg_id, hull_coordinates in grouped_coordinates.items():
        feature = {
            "type": "Feature",
            "properties": {
                "RegID": reg_id
            },
            "geometry": {
                "type": "Polygon",
                "coordinates": [hull_coordinates]
            }
        }
        geojson_result["features"].append(feature)

    return jsonify(geojson_result)


if __name__ == '__main__':
    app.run(debug=True)
